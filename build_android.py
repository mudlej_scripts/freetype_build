import os
import shutil
from common import run_cmd, log, error, FREETYPE, delete_if_exists

# Toolchain path
NDK_PATH = "/opt/android-ndk/"
TOOLCHAIN = os.path.join(NDK_PATH, "build/cmake/android.toolchain.cmake")
ANDROID_PLATFORM = "19"

# Install path
# INSTALL_PREFIX_OS = os.path.join(cmn.INSTALL_PREFIX, "android")
INSTALL_PATH = "../../lib/"
LIBPNG_PATH = "../../../libpng_build"

def run():
    log("Start " + __file__)
    log("INSTALL_PATH: " + INSTALL_PATH)

    build("x86_64", "Release")
    build("arm64-v8a", "Release")
    build("x86", "Release")
    build("armeabi-v7a", "Release")

def build(arch, build_type):
    """
    arch        ->  "x86_64",  "arm64-v8a", ...
    build_type  ->  "Debug" or "Release"
    """
    log("Building libpng arch=" + arch + " build_type=" + build_type)
    INSTALL_PREFIX = os.path.join(INSTALL_PATH, arch)
    os.chdir(FREETYPE)

    # remove existing builds
    shutil.rmtree("build", ignore_errors=True)

    # create build folder
    os.makedirs("build", exist_ok=True)
    os.chdir("build")

    # libpng dependency paths
    LIBPNG_INCLUDE_PATH = os.path.abspath(os.path.join(os.getcwd(), LIBPNG_PATH, "lib", arch, "include"))
    LIBPNG_LIBRARY_PATH = os.path.abspath(os.path.join(os.getcwd(), LIBPNG_PATH, "lib", arch, "lib", "libpng16.a"))
    print(LIBPNG_INCLUDE_PATH)
    print(LIBPNG_LIBRARY_PATH)
    # exit(0)

    # cmake generator
    cmd = ["cmake"]
    cmd += ["-DCMAKE_POSITION_INDEPENDENT_CODE=ON"]
    cmd += ["-DCMAKE_TOOLCHAIN_FILE=" + TOOLCHAIN]
    cmd += ["-DBUILD_SHARED_LIBS=true"]
    cmd += ["-DANDROID_ABI=" + arch]
    cmd += ["-DANDROID_PLATFORM=" + ANDROID_PLATFORM]
    cmd += ["-DCMAKE_INSTALL_PREFIX=" + INSTALL_PREFIX]
    cmd += ["-DFT_WITH_ZLIB=ON -D FT_WITH_PNG=ON"]
    cmd += ["-DPNG_PNG_INCLUDE_DIR=" + LIBPNG_INCLUDE_PATH]
    cmd += ["-DPNG_LIBRARY=" + LIBPNG_LIBRARY_PATH]
    cmd += [".."]
    run_cmd(cmd)

    # cmake build
    cmd = ["cmake --build ."]
    cmd += ["--config " + build_type]
    run_cmd(cmd)

    # cmake install
    cmd = ["cmake --install ."]
    cmd += ["--config " + build_type]
    run_cmd(cmd)

    log(f"Finished building {arch}")

    # move the shared lib 
    shutil.move("libfreetype.so", f"../../../../lib/{arch}/libmodft2.so")

    # get back to the main dir
    os.chdir("../../")

    # remove other builds
    delete_if_exists(f"{FREETYPE}/build")

if __name__ == '__main__':
    run()
