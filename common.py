import subprocess
import requests
import tarfile
import shutil
import os

FREETYPE = "freetype-2.12.1"
VERSION = "2-12-1"

TOP_DIR = os.getcwd()
INSTALL_PREFIX = os.path.abspath(os.path.join(TOP_DIR, "..", "library"))
FREETYPE_DIR = os.path.join(TOP_DIR, FREETYPE)

#------------------------------------------------------------
def run_cmd(cmd):
    command = " ".join(cmd)
    log("Run: " + command)
    result = subprocess.call(command, shell=True)
    if result != 0:
        error(command + "  result=" + str(result))


def error(msg):
    log("Error !!! " + msg)
    os.sys.exit(0)


def log(msg):
    print("* " + msg)


def delete_if_exists(path):
    if os.path.exists(path):
        shutil.rmtree(path)


def download_file(url, filename=None):
    if not filename:
        filename = url.split('/')[-1] + ".tar.xz"
    
    print(f"* Downloading {filename}")
    with requests.get(url, stream=True) as request:
        request.raise_for_status()
        
        with open(filename, 'wb') as file:
            for chunk in request.iter_content(chunk_size=1024): 
                file.write(chunk)

    print(f"* Finished downloading {filename}")
    return filename


def extract_tar_file(filename, path="."):
    print(f"* Extracting {filename}")
    with tarfile.open(filename) as file:
        file.extractall(path)
    print(f"* Finished extracting {filename}")