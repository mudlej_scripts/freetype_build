from common import delete_if_exists, FREETYPE
import prepare, build_android

prepare.run()
build_android.run()
delete_if_exists(FREETYPE)
